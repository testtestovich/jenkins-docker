package com.example.demo.controller;

import com.example.demo.entity.Organization;
import com.example.demo.entity.Worker;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @PostMapping(value = "/validateWorker", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<String> validate(@Valid @RequestBody Worker worker, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<String> errorList = new ArrayList<>();
            for (Object object : bindingResult.getAllErrors()) {
                FieldError fieldError = (FieldError) object;
                String errorDescription = "field '" + fieldError.getField() + "' " + fieldError.getDefaultMessage() + "";
                errorList.add(errorDescription);
            }
            return ResponseEntity.badRequest().body("{\"message\": \"Malformed request: " + errorList.toString().replaceAll("\\[|\\]", "") + "\"}");
        }
        return ResponseEntity.ok("validWorker");
    }

    @PostMapping("/validateOrganization")
    ResponseEntity<String> validate(@Valid @RequestBody Organization organization) {
        return ResponseEntity.ok("validOrganization");
    }

    @GetMapping("/test")
    String returnMessage() {
        return "LOL KEK CHEBUREK";
    }
}