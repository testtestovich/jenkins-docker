package com.example.demo.entity;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class Organization {

    @NotEmpty
    private List<Worker> workerList;

    @NotNull
    private String orgName;

    @Max(2000)
    private int establishmentYear;

    public Organization() {
    }

    public Organization(@NotEmpty List<Worker> workerList, @NotNull String orgName, @Max(2000) int establishmentYear) {
        this.workerList = workerList;
        this.orgName = orgName;
        this.establishmentYear = establishmentYear;
    }

    public List<Worker> getWorkerList() {
        return workerList;
    }

    public void setWorkerList(List<Worker> workerList) {
        this.workerList = workerList;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getEstablishmentYear() {
        return establishmentYear;
    }

    public void setEstablishmentYear(int establishmentYear) {
        this.establishmentYear = establishmentYear;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "workerList=" + workerList +
                ", orgName='" + orgName + '\'' +
                ", establishmentYear=" + establishmentYear +
                '}';
    }
}
